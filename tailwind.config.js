const colors = require("tailwindcss/colors")

module.exports = {
    purge: {
        content: ["./src/**/*.tsx", "./src/**/*.ts", "./public/index.html"]
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                white: {
                    normal: "#FFFFFF",
                    "modal-light": "#FFFFFF"
                },
                blue: {
                    100: "#f5f5ff",
                    600: "#4848ff",
                    "navbar-mobile-active": "#3F3FD5",
                    "navbar-hover": "rgb(244, 244, 244)",
                    "navbar-active-hover": "#4848ff12"
                },
                green: {
                    500: "#6ece56"
                },
                gray: {
                    50: "#f0f0f0",
                    70: "#afafaf",
                    100: "#f7f7f7",
                    200: "#dddfe2",
                    300: "#ccd0d5",
                    400: "#9a9a9a",
                    500: "#676767"
                },
                red: {
                    error: "#f02849",
                    like: "#ff4848",
                    light: "rgba(240,40,73,.1607843137254902)"
                }
            },
            fontSize: {
                "4xl": "2.5rem"
            },
            fontWeight: {
                default: 450
            },
            container: {
                center: true
            },
            width: {
                auth: "396px"
            },
            boxShadow: {
                default: "0 4px 8px 0 rgba(0,0,0,.2)"
            },
            spacing: {
                0.5: "5px",
                1.2: "11px",
                1.5: "12px",
                2: "15px"
            }
        }
    },
    variants: {
        extend: {}
    },
    plugins: []
}
