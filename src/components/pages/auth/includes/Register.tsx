import React, { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { Link, Redirect } from "react-router-dom"
import { checkUserAuth } from "../../../../api/auth"
import { ISelectTeam } from "../../../../models/Interfaces"
import Loading from "../../../../utils/Loading"
import { registerRequest, allTeamRequest } from "./Requests"

const Register: React.FC = () => {
    const [firstName, setFirstName] = useState<string>("")
    const [lastName, setLastName] = useState<string>("")
    const [job, setJob] = useState<string>("")
    const [team, setTeam] = useState<string>("")
    const [allTeam, setAllTeam] = useState<ISelectTeam[]>([])
    const [email, setEmail] = useState<string>("")
    const [password, setPassword] = useState<string>("")
    const [confirmPassword, setConfirmPassword] = useState<string>("")
    const [isRegister, setIsRegister] = useState<boolean>(false)
    const [isLogin, setIsLogin] = useState<boolean>(false)
    const [isError, setIsError] = useState<boolean>(false)
    const [errorMsg, setErrorMsg] = useState<string>("")
    const [isSubmit, setIsSubmit] = useState<boolean>(false)
    const { register, errors, handleSubmit, getValues } = useForm()

    const validatePassword = (value: any) => {
        return value === getValues("password")
    }

    useEffect(() => {
        if (checkUserAuth()) {
            setIsLogin(true)
        }
    }, [])

    useEffect(() => {
        allTeamRequest().then(res => {
            if (res.data.status === "success") {
                const { data } = res.data
                const result: ISelectTeam[] = []

                data.forEach((item: string) => {
                    result.push({ value: item, label: item })
                })
                setAllTeam(result)
            }
        })
    }, [])

    const handlerChangeValue = (e: any) => {
        const { name, value } = e.target
        switch (name) {
        case "email":
            setEmail(value)
            break
        case "password":
            setPassword(value)
            break
        case "confirmPassword":
            setConfirmPassword(value)
            break
        case "firstName":
            setFirstName(value)
            break
        case "lastName":
            setLastName(value)
            break
        case "job":
            setJob(value)
            break
        case "team":
            setTeam(value)
            break
        default:
            break
        }
        setIsError(false)
        setErrorMsg("")
    }

    const onSubmit = () => {
        setIsSubmit(true)
        registerRequest(email, password, firstName, lastName, job, team).then(res => {
            if (res.data.status === "success") {
                setIsRegister(true)
            } else {
                setIsError(true)
                setErrorMsg(res.data.message)
                setIsSubmit(false)
            }
        }).catch(err => {
            setIsError(true)
            setErrorMsg(err.message)
            setIsSubmit(false)
        })
    }

    if (isRegister) {
        return (
            <Redirect to="/login" />
        )
    }

    if (isLogin) {
        return (
            <Redirect to="/timelines" />
        )
    }

    return (
        <div className="sm:container sm:w-auth px-2 sm:py-0.5 sm:bg-white-normal sm:shadow-default sm:border sm:border-gray-200 sm:rounded-xl">
            <p className="text-center my-0.5 sm:my-2 font-medium text-lg">S'inscrire à Reactly</p>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="text"
                        name="lastName"
                        value={lastName}
                        autoFocus
                        onChange={handlerChangeValue}
                        placeholder="Nom"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.lastName && "#f02849" }}

                    />
                    {errors.lastName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom</p>}
                </div>
                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="text"
                        name="firstName"
                        value={firstName}
                        onChange={handlerChangeValue}
                        placeholder="Prénom"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.firstName && "#f02849" }}

                    />
                    {errors.firstName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un prénom</p>}
                </div>

                <div>
                    <select
                        value={team}
                        name="team"
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none cursor-pointer"
                        onChange={handlerChangeValue}
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.team && "#f02849" }}
                    >
                        <option value="" disabled hidden>Choisissez une équipe</option>
                        {!allTeam.length && <option value="none" disabled>Aucune équipe</option>}
                        {allTeam.map((item: ISelectTeam) => {
                            return (
                                <option key={item.value} value={item.value}>{item.label}</option>
                            )
                        })}
                    </select>

                    {errors.team && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir une équipe</p>}
                </div>

                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="text"
                        name="job"
                        value={job}
                        onChange={handlerChangeValue}
                        placeholder="Métier"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.job && "#f02849" }}

                    />
                    {errors.job && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un métier</p>}
                </div>
                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="email"
                        name="email"
                        value={email}
                        onChange={handlerChangeValue}
                        placeholder="Adresse e-mail"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.email && "#f02849" }}

                    />
                    {errors.email && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un email</p>}
                </div>
                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="password"
                        name="password"
                        value={password}
                        onChange={handlerChangeValue}
                        placeholder="Mot de passe"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.password && "#f02849" }}
                    />
                    {errors.password && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un mot de passe</p>}
                </div>
                <div>
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={handlerChangeValue}
                        placeholder="Confirmation de mot de passe"
                        ref={register && register({ required: true, validate: validatePassword })}
                        style={{ borderColor: errors.confirmPassword && "#f02849" }}
                    />
                    {errors.confirmPassword && <p className="text-sm text-red-error mb-1 pl-1">La confirmation doit être identique au mot de passe</p>}
                </div>
                <input className="w-full text-white-normal bg-blue-600 rounded-md font-bold text-xl py-1.5 my-0.5 cursor-pointer" type="submit" value="Inscription" />
                {isError && <p className="w-full text-center py-1.5 px-0.5 mt-0.5 rounded-md text-red-error bg-red-light">{errorMsg}</p>}
                <div className="flex justify-center items-center m-3">
                    <p className="text-lg sm:text-base">Déjà inscrit ? </p>
                    <Link to="/login" className="ml-1 font-bold text-green-500 text-lg sm:text-base">Se connecter</Link>
                </div>

            </form>
            {isSubmit && <Loading />}
        </div>
    )
}

export default Register