import { loginApi, registerApi, allTeamApi } from "../../../../api/ApiRequest"

export const loginRequest = async (email: string, password: string) => {
    return await loginApi(email, password).then(res => {
        return res
    }).catch(err => {
        if (err.data.message === "invalid credentials") {
            err.data.message = "Adresse e-mail ou mot de passe incorrecte."
        } else {
            err.data.message = "Une erreur est survenue. Veuillez réessayer."
        }
        return err
    })
}

export const registerRequest = async (email: string, password: string, firstName: string, lastName: string, job: string, team: string) => {
    return await registerApi(email, password, firstName, lastName, job, team).then(res => {
        return res
    }).catch(err => {
        if (err.data.message === "Invalid email") {
            err.data.message = "Adresse e-mail invalide."
        } else if (err.data.message === "Email already exists") {
            err.data.message = "Adresse e-mail déjà utilisée pour un autre compte."
        } else if (err.data.message === "Invalid team") {
            err.data.message = "L'équipe choisie est invalide."
        } else {
            err.data.message = "Une erreur est survenue. Veuillez réessayer."
        }
        return err
    })
}

export const allTeamRequest = async () => {
    return await allTeamApi().then(res => {
        return res
    }).catch(err => {
        err.data.message = "Une erreur est survenue. Veuillez réessayer."
        return err
    })
}
