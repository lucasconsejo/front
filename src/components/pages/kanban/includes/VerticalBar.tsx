/* eslint-disable @typescript-eslint/no-non-null-asserted-optional-chain */
import React, { useContext, useEffect, useState } from "react"
import { useHistory } from "react-router"
import { useForm } from "react-hook-form"
import { v4 as uuidv4 } from "uuid"
import firebase from "../../../../firebase/firebase"
import { useWindowResolution } from "../../../../hooks/CustomHooks"
import { UserContext } from "../../../../context/userContext"
import { TablesContext } from "../../../../context/tablesContext"
import { capitalize } from "../../../../utils/Utils"
import ModalTemplate from "../../../../utils/ModalTemplate"

type PropsType = {
    tableIndex: number,
    setTableIndex: any,
    setOpenMenu: any
}

const VerticalBar: React.FC<PropsType> = ({ tableIndex, setTableIndex, setOpenMenu }) => {
    const { width } = useWindowResolution()
    const history = useHistory()
    const { tablesState, tablesDispatch } = useContext(TablesContext)
    const [showModal, setShowModal] = useState<boolean>(false)
    const [openBtn, setOpenBtn] = useState<boolean>(true)
    const { userState } = useContext(UserContext)

    const db = firebase.firestore()

    const { handleSubmit, register, errors } = useForm()

    useEffect(() => {
        setOpenMenu(!openBtn)
    }, [openBtn])

    const onSubmit = async ({ tableName }: any) => {
        const _id = uuidv4()
        db.collection("tables").doc(_id).set({
            name: capitalize(tableName),
            teamId: userState.user?.teamId,
            subTable: [],
            tasks: []
        })
        tablesDispatch({
            type: "ADD_TABLE",
            payload: { _id, name: capitalize(tableName), teamId: userState.user?.teamId, subTable: [], tasks: [] }
        })
        setShowModal(false)
    }

    if (openBtn) {
        return (
            <div className="flex flex-col justify-between lg:pt-7 px-2 h-screen pb-7 bg-white-normal shadow z-49 fixed">
                <div>
                    <div>
                        <h2 className="pt-12 ml-1.5 sm:ml-0 text-lg lg:text-2xl font-bold text-gray-500">Kanban</h2>
                        <h3 className="text-lg lg:text-lg font-medium text-gray-500">{userState.user?.team}</h3>
                    </div>

                    {!tablesState.tables.length && <p className="my-2 lg:text-lg font-medium">Aucun tableau disponible</p>}
                    {
                        tablesState.tables.map((item, idx) => {
                            return (
                                <div key={item._id} onClick={() => { setTableIndex(idx); history.push(item._id) }} className="mt-2 flex space-x-1.2 cursor-pointer items-center">
                                    <div className={`lg:text-lg font-medium ${tableIndex === idx ? "text-white-normal bg-blue-600" : "text-gray-500 bg-gray-200"} px-1.2 py-1 rounded`}>
                                        <p>{item.name.split("")[0]}</p>
                                    </div>
                                    <p className={`lg:text-lg font-medium ${tableIndex === idx ? "text-blue-600" : " text-gray-500"} `}>{item.name}</p>
                                </div>
                            )
                        })
                    }

                    <div onClick={() => setShowModal(true)} className="bg-green-500 cursor-pointer w-full mt-2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 text-center">
                        <div className="flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                            </svg>
                            <p>Créer un Tableau</p>
                        </div>
                    </div>

                    {showModal ? (
                        <ModalTemplate title="Créer un tableau" showModal={setShowModal}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="relative py-2 flex-auto">
                                    {/* Header */}
                                    <p className="pt-2 text-lg">Nom du tableau</p>
                                    <input
                                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                        type="text"
                                        name="tableName"
                                        defaultValue=""
                                        autoFocus
                                        placeholder="Nom du tableau"
                                        ref={register && register({ required: true })}
                                        style={{ borderColor: errors.tableName && "#f02849" }}
                                    />
                                    {errors.tableName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom de tableau</p>}
                                </div>
                                {/* footer */}
                                <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Créer le tableau</button>
                            </form>
                        </ModalTemplate>
                    ) : null}
                </div>

                <div onClick={() => setOpenBtn(!openBtn)} className="flex space-x-1.5 cursor-pointer text-gray-500">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
                    </svg>
                    <p className="font-medium">Fermer</p>
                </div>
            </div>
        )
    } else {
        return (
            <div className="flex flex-col justify-between lg:pt-7 px-2 h-screen pb-7 bg-white-normal shadow z-49 fixed text-gray-500 items-center" style={{ width: "60px" }}>
                <div className="pt-9">
                    {
                        tablesState.tables.map((item, idx) => {
                            return (
                                <div key={item._id} onClick={() => { setTableIndex(idx); history.push(item._id) }} className="mt-1.5 flex cursor-pointer">
                                    <div className={`lg:text-lg font-medium ${tableIndex === idx ? "text-white-normal bg-blue-600" : " text-gray-500 bg-gray-200"} text-center w-full py-1.2 rounded`}>
                                        <p>{item.name.split("")[0]}</p>
                                    </div>
                                </div>
                            )
                        })
                    }

                    <div onClick={() => setShowModal(true)} className="bg-green-500 cursor-pointer w-full mt-2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </div>
                </div>

                <div onClick={() => setOpenBtn(!openBtn)} className="cursor-pointer">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                    </svg>
                </div>

                {showModal ? (
                    <ModalTemplate title="Créer un tableau" showModal={setShowModal}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="relative py-2 flex-auto">
                                {/* Header */}
                                <p className="pt-2 text-lg">Nom du tableau</p>
                                <input
                                    className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                    type="text"
                                    name="tableName"
                                    defaultValue=""
                                    autoFocus
                                    placeholder="Nom du tableau"
                                    ref={register && register({ required: true })}
                                    style={{ borderColor: errors.tableName && "#f02849" }}
                                />
                                {errors.tableName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom de tableau</p>}
                            </div>
                            {/* footer */}
                            <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Créer le tableau</button>
                        </form>
                    </ModalTemplate>
                ) : null}
            </div>
        )
    }
}

export default VerticalBar