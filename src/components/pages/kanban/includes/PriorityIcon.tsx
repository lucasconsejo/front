import React from "react"

import BlockerIcon from "../../../../assets/img/kanban/priority/blocker.svg"
import CriticalIcon from "../../../../assets/img/kanban/priority/critical.svg"
import HighIcon from "../../../../assets/img/kanban/priority/high.svg"
import HighestIcon from "../../../../assets/img/kanban/priority/highest.svg"
import LowIcon from "../../../../assets/img/kanban/priority/low.svg"
import LowestIcon from "../../../../assets/img/kanban/priority/lowest.svg"
import MajorIcon from "../../../../assets/img/kanban/priority/major.svg"
import MediumIcon from "../../../../assets/img/kanban/priority/medium.svg"
import MinorIcon from "../../../../assets/img/kanban/priority/minor.svg"
import TrivialIcon from "../../../../assets/img/kanban/priority/trivial.svg"

type PropsType = {
    priority: string
}

const PriorityIcon: React.FC<PropsType> = ({ priority }) => {
    let result = MediumIcon

    switch (priority) {
    case "blocker":
        result = BlockerIcon
        break
    case "critical":
        result = CriticalIcon
        break
    case "highest":
        result = HighestIcon
        break
    case "high":
        result = HighIcon
        break
    case "low":
        result = LowIcon
        break
    case "lowest":
        result = LowestIcon
        break
    case "major":
        result = MajorIcon
        break
    case "medium":
        result = MediumIcon
        break
    case "minor":
        result = MinorIcon
        break
    case "trivial":
        result = TrivialIcon
        break
    default:
        break
    }

    return <img src={result} alt="priority" className="w-6 h-6" />
}

export default PriorityIcon