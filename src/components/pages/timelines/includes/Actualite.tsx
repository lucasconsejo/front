import React, { useContext } from "react"
import PublishedPost from "./PublishedPost"
import AddPost from "./AddPost"
import { PostsContext } from "../../../../context/postsContext"
import { useWindowResolution } from "../../../../hooks/CustomHooks"

const Actualite: React.FC = () => {
    const { postsState, postsDispatch } = useContext(PostsContext)
    const { width } = useWindowResolution()

    const handleMorePost = () => {
        postsDispatch({
            type: "LOAD_MORE_POST",
            payload: true
        })
    }

    return (
        <div className="container mx-auto sm:w-2/3 md:w-4/6 lg:w-2/5 lg:pt-7 pb-7">
            {(width > 640) && <h2 className="pt-12 mb-1 ml-1.5 sm:ml-0 text-lg lg:text-2xl font-bold text-gray-500">Actualités</h2>}
            <AddPost />
            {
                postsState.posts?.map((item, index) => {
                    return <PublishedPost key={item._id} post={item} />
                })
            }
            {postsState.hasNextPage ?
                <div onClick={() => handleMorePost()} className="text-center text-blue-500 font-medium cursor-pointer mt-2 hover:underline">Charger plus de publications</div>
                : <div className="text-center text-gray-500 font-medium mt-2">Fin des publications</div>
            }
        </div>
    )
}

export default Actualite
