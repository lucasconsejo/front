/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/self-closing-comp */
import React, { useCallback, useContext, useEffect, useRef, useState } from "react"
import { useForm } from "react-hook-form"
import Gallery from "react-photo-gallery"
import useSmoothScroll from "react-smooth-scroll-hook"
import Carousel, { Modal, ModalGateway } from "react-images"
import { getAllPost } from "../../../../api/ApiRequest"
import { PostsContext } from "../../../../context/postsContext"
import { UserContext } from "../../../../context/userContext"
import { capitalize } from "../../../../utils/Utils"
import { newPostRequest } from "./Requests"
import AddImageIcon from "../../../../assets/img/posts/ic_add_img.svg"
import ViewIcon from "../../../../assets/img/posts/ic_view.svg"
import DeleteIcon from "../../../../assets/img/posts/ic_delete.svg"
import LeftArrowIcon from "../../../../assets/img/posts/ic_left_arrow.svg"
import RightArrowIcon from "../../../../assets/img/posts/ic_right_arrow.svg"
import ModalTemplate from "../../../../utils/ModalTemplate"
import "../../../../assets/css/overlay-render.css"
import Loading from "../../../../utils/Loading"
import { useWindowResolution } from "../../../../hooks/CustomHooks"

const AddPost: React.FC = () => {
    const [showModal, setShowModal] = React.useState(false)
    const [description, setDescription] = useState<string>("")
    const [images, setImages] = useState<any>([])
    const [selectedImage, setSelectedImage] = useState<any>([])
    const [currentImage, setCurrentImage] = useState<number>(0)
    const [viewerIsOpen, setViewerIsOpen] = useState<boolean>(false)
    const [canPost, setCanPost] = useState<boolean>(false)
    const [deleteOption, setDeleteOption] = useState<boolean>(false)
    const [isSubmit, setIsSubmit] = useState<boolean>(false)
    const { width } = useWindowResolution()
    const { userState, userDispatch } = useContext(UserContext)
    const { postsState, postsDispatch } = useContext(PostsContext)
    const ref = useRef(null)
    const { scrollTo } = useSmoothScroll({
        ref,
        speed: 15,
        direction: "x"
    })
    const { handleSubmit } = useForm()

    useEffect(() => {
        if (selectedImage.length <= 0) setDeleteOption(false)
    }, [selectedImage])

    useEffect(() => {
        if (!showModal) {
            setDescription("")
            setImages([])
            setSelectedImage([])
            setDeleteOption(false)
        }
    }, [showModal])

    useEffect(() => {
        if (description.length > 0 || selectedImage.length > 0) setCanPost(true)
        else if (description.length <= 0 && selectedImage.length <= 0) setCanPost(false)
    }, [description, selectedImage])

    const imgHandleChange = (e: any) => {
        if (e.target.files) {
            const fileArray = Array.from(e.target.files).map((file: any) => {
                const arrTmp = images
                arrTmp.push(file)
                setImages(arrTmp)
                return URL.createObjectURL(file)
            })
            fileArray.forEach((photo: any) => {
                setSelectedImage((prevImages: any) => prevImages.concat({
                    src: photo,
                    width: 1,
                    height: 1
                }))
            })
            Array.from(e.target.files).map((file: any) => URL.revokeObjectURL(file))
        }
    }

    const deletePhoto = (photo: string) => {
        const getIndex = (item: any) => item.src === photo
        const index = selectedImage.findIndex(getIndex)
        if (index > -1) {
            const arrTmp = images
            arrTmp.splice(index, 1)
            setImages(arrTmp)
            setSelectedImage(selectedImage.filter((item: any) => item.src !== photo))
        }
    }

    const openLightbox = useCallback((event, { photo, index }) => {
        if (!deleteOption) {
            setCurrentImage(index)
            setViewerIsOpen(true)
        }
        else {
            deletePhoto(photo.src)
        }
    }, [deleteOption, selectedImage])

    const closeLightbox = () => {
        setCurrentImage(0)
        setViewerIsOpen(false)
    }

    const ImageRender = ({ index, onClick, photo, margin }: any) => (
        <div>
            <div className="content">
                <div>
                    <div className="content-overlay"></div>
                    <div style={{ margin, width: photo.width, height: "150px", backgroundImage: `url(${photo.src})`, backgroundSize: "cover" }}>
                        <span></span>
                    </div>
                    <div className="flex mb-1 content-details fadeIn-bottom justify-center">
                        <div className="flex cursor-pointer border rounded-md border-white-normal justify-center p-1.2 mr-1.5" onClick={e => onClick(e, { index, photo })}>
                            <img src={ViewIcon} alt="img" className="w-8 h-8 sm:w-8 sm:h-8" />
                        </div>
                        <div className="flex cursor-pointer border rounded-md border-white-normal justify-center p-1.2" onClick={e => deletePhoto(photo.src)}>
                            <img src={DeleteIcon} alt="img" className="w-7 h-7 sm:w-7 sm:h-7" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    const onSubmit = () => {
        setIsSubmit(true)
        const formData = new FormData()
        formData.append("description", description)
        for (const file of images) {
            formData.append("images", file)
        }
        newPostRequest(formData).then(res => {
            postsDispatch({
                type: "ADD_POST",
                payload: res.data.data
            })
            setViewerIsOpen(false)
            setDescription("")
            setSelectedImage([])
            setImages([])
            setShowModal(false)
        }).finally(() => setIsSubmit(false))
    }

    return (
        <>
            <form className={`flex justify-between bg-white-normal items-center sm:shadow-default sm:border sm:border-gray-200 sm:rounded-xl ${width <= 640 ? "pt-12 pb-1" : "py-1"} px-1.2 sm:py-1.5`} onSubmit={handleSubmit(onSubmit)}>
                <img src={`${userState.user?.image}`} className="w-9 h-9 sm:w-10 sm:h-10 rounded-full" alt="Timeline icon" />
                <div className="flex-1 mx-1.2">
                    <div
                        className="w-full bg-blue-100 sm:bg-gray-100 p-1.2 text-sm border border-gray-300 sm:border-gray-50 sm:border-2 rounded-md font-medium outline-none cursor-pointer"
                        onClick={() => setShowModal(true)}
                    >
                        <p className="text-gray-500">{`${width <= 640 ? "Que voulez-vous dire" : "Voulez-vous dire quelque chose"}, ${userState.user?.firstName} ?`}</p>
                    </div>
                </div>

                {showModal ? (
                    <ModalTemplate title="Ajouter un post" showModal={setShowModal}>
                        <>
                            <div className="relative pt-2 flex-auto">
                                {/* Header */}
                                <div className="flex w-full">
                                    <img src={`${userState.user?.image}`} className="w-9 h-9 sm:w-10 sm:h-10 rounded-full" alt="user profil" width="40" height="40" />
                                    <div className="flex flex-col ml-1 leading-5">
                                        <div className="flex">
                                            <h3 className="text-gray-500">{capitalize(userState.user?.firstName)} {capitalize(userState.user?.lastName)}</h3>
                                        </div>
                                        <p className="text-gray-400">Équipe {userState.user?.team} - {userState.user?.job}</p>
                                    </div>
                                </div>
                                {/* Text input */}
                                <textarea
                                    rows={5}
                                    name="post"
                                    value={description}
                                    className="w-full p-1 mt-2 text-xl font-medium outline-none resize-none "
                                    placeholder={`${width <= 640 ? "Que voulez-vous dire" : "Voulez-vous dire quelque chose"}, ${userState.user?.firstName} ?`}
                                    onChange={e => setDescription(e.target.value)}
                                />
                                {/* Gallery images */}
                                <div>
                                    {selectedImage.length > 0 ? (<p className="text-sm text-gray-600">{selectedImage.length} image{selectedImage.length > 1 ? "s" : ""}</p>) : null}
                                    <div className="flex">
                                        {selectedImage.length > 2 ? (
                                            <div className="flex cursor-pointer justify-center" onClick={() => scrollTo(-225)}>
                                                <img src={LeftArrowIcon} alt="left arrow" />
                                            </div>
                                        ) : null}

                                        <div className={`${selectedImage.length > 2 ? "w-11/12" : "w-full"}`}>
                                            <div style={{ overflowX: "scroll" }} className="overflow-hidden mb-1.5" ref={ref}>
                                                <Gallery photos={selectedImage} columns={selectedImage.length > 2 ? 2 : selectedImage.length} onClick={openLightbox} ImageComponent={ImageRender} />
                                            </div>
                                            <ModalGateway>
                                                {viewerIsOpen ? (
                                                    <Modal onClose={closeLightbox}>
                                                        <Carousel
                                                            currentIndex={currentImage}
                                                            views={selectedImage.map((x: any) => ({
                                                                ...x
                                                            }))}
                                                        />
                                                    </Modal>
                                                ) : null}
                                            </ModalGateway>
                                        </div>
                                        {selectedImage.length > 2 ? (
                                            <div className="flex cursor-pointer justify-center" onClick={() => scrollTo(225)}>
                                                <img src={RightArrowIcon} alt="right arrow" />
                                            </div>
                                        ) : null}
                                    </div>
                                </div>
                            </div>
                            {/* footer */}
                            <div className="items-center mb-2">
                                {/* input add images */}
                                <div>
                                    <input type="file" multiple id="file" className="hidden" onChange={imgHandleChange} />
                                    <div className="label-holder">
                                        <label htmlFor="file" className="label flex cursor-pointer border border-gray-300 rounded-md justify-center p-1.2">
                                            <img src={AddImageIcon} alt="img" className="w-5 h-5 sm:w-6 sm:h-6" />
                                            <p className="ml-1">Ajouter une image</p>
                                        </label>
                                    </div>
                                </div>
                                <button className={`${!canPost ? "bg-gray-300 cursor-not-allowed" : "bg-blue-600 cursor-pointer"} w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2`} disabled={!canPost} type="submit">Publier</button>
                            </div>
                        </>
                    </ModalTemplate>
                ) : null}
            </form>
            {isSubmit && <Loading />}
        </>
    )
}

export default AddPost
