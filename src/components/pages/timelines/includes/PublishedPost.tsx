import React, { useContext, useState } from "react"
import { UserContext } from "../../../../context/userContext"
import LikeSection from "./LikeSection"
import HeaderSection from "./HeaderSection"
import { PostType } from "../../../../models/Types"
import DescriptionSection from "./DescriptionSection"
import CommentsList from "./CommentsList"
import { deleteOnePost, getAllPost, likePostApi } from "../../../../api/ApiRequest"
import { PostsContext } from "../../../../context/postsContext"
import ModalTemplate from "../../../../utils/ModalTemplate"
import Loading from "../../../../utils/Loading"

type Props = {
    post: PostType,
    shared?: boolean
}

const PublishedPost: React.FC<Props> = ({ post, shared = false }) => {
    const [isLiked, setIsLiked] = useState<boolean>(post.currentUserLiked)
    const [isOpenComment, setIsOpenComment] = useState<boolean>(shared)
    const [nbLike, setNbLike] = useState<number>(post.nbLike)
    const [showModal, setShowModal] = useState<boolean>(false)
    const [isSubmit, setIsSubmit] = useState<boolean>(false)
    const { userState, userDispatch } = useContext(UserContext)
    const { postsState, postsDispatch } = useContext(PostsContext)

    const handlerLikeBtn = () => {
        if (isLiked) {
            setIsLiked(false)
            setNbLike(nbLike - 1)
        } else {
            setIsLiked(true)
            setNbLike(nbLike + 1)
        }

        likePostApi(post._id).then(res => {
            postsDispatch({
                type: "UPDATE_POST",
                payload: {
                    postId: post._id,
                    post: res.data.data
                }
            })
        })
    }

    const handlerOpenComment = () => {
        if (isOpenComment) {
            setIsOpenComment(false)
        } else {
            setIsOpenComment(true)
        }
    }

    const handlerDeleteBtn = () => !showModal && setShowModal(true)

    const deletePost = () => {
        setIsSubmit(true)
        if (post.user.id === userState.user?._id) {
            deleteOnePost(post._id).then(res => {
                postsDispatch({
                    type: "DELETE_POST",
                    payload: post._id
                })
                setShowModal(false)
            }).finally(() => setIsSubmit(false))
        }
    }

    return (
        <>
            <div className="bg-white-normal sm:shadow-default sm:border sm:border-gray-200 sm:rounded-xl p-1.2 mt-1.2 sm:mt-5">
                <HeaderSection post={post} isLiked={isLiked} nbLike={nbLike} handlerLikeBtn={handlerLikeBtn} handlerDeleteBtn={handlerDeleteBtn} />
                <DescriptionSection description={post.description} images={post.images} />
                <LikeSection isLiked={isLiked} isOpenComment={isOpenComment} nbComment={post.comments.length} handlerLikeBtn={handlerLikeBtn} handlerOpenComment={handlerOpenComment} postId={post._id} />
                {isOpenComment ? <CommentsList comments={post.comments} postId={post._id} /> : null}

                {showModal ? (
                    <ModalTemplate title="Supprimer le post" showModal={setShowModal}>
                        <>
                            <div className="relative pt-2 flex-auto">
                                {/* Header */}
                                <p className="py-1.2 text-lg">Êtes-vous sûr de vouloir supprimer ce post ?</p>
                            </div>
                            {/* footer */}
                            <div className="items-center mb-2 flex">
                                <button className="bg-gray-300 cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 mr-1" onClick={() => setShowModal(false)}>Annuler</button>
                                <button className="bg-red-error cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" onClick={() => deletePost()}>Supprimer</button>
                            </div>
                        </>
                    </ModalTemplate>
                ) : null}
            </div>
            {isSubmit && <Loading />}
        </>
    )
}

export default PublishedPost
