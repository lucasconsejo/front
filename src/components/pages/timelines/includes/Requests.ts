import { newPostApi, postComment } from "../../../../api/ApiRequest"

export const newPostRequest = async (formData: FormData) => {
    return await newPostApi(formData).then(res => {
        return res
    }).catch(err => {
        return err
    })
}

export const newComment = async (comment: string, postId: string) => {
    return await postComment(comment, postId).then(res => {
        return res
    }).catch(err => {
        return err
    })
}
