import React, { useContext, useState } from "react"
import { useForm } from "react-hook-form"
import TimelineActiveIcon from "../../../../assets/img/navbar/pc/ic_timeline_active.svg"
import { UserContext } from "../../../../context/userContext"
import { newPostRequest } from "./Requests"
import { PostsContext } from "../../../../context/postsContext"
import { getAllPost } from "../../../../api/ApiRequest"

const NoActualite: React.FC = () => {
    const [description, setDescription] = useState<string>("")
    const { userState, userDispatch } = useContext(UserContext)
    const { postsState, postsDispatch } = useContext(PostsContext)
    const { register, errors, handleSubmit } = useForm()

    const onSubmit = () => {
        const formData = new FormData()
        formData.append("description", description)
        newPostRequest(formData).then(() => {
            getAllPost(0).then(res => {
                postsDispatch({
                    type: "INIT_POST",
                    payload: res.data.data
                })
            })
            setDescription("")
        })
    }

    return (
        <div className="container mx-auto sm:w-2/3 md:w-4/6 lg:w-2/5 py-11">
            <div className="flex justify-center items-center flex-col sm:mt-12 bg-white-normal sm:shadow-default sm:border sm:border-gray-200 sm:rounded-xl p-2">
                <img src={TimelineActiveIcon} alt="Timeline icon" width="40" height="40" />
                <h2 className="mt-1 mb-1.5 text-lg">Aucune actualité</h2>
                <form className="flex flex-col w-full sm:w-9/12" onSubmit={handleSubmit(onSubmit)}>
                    <div className="flex justify-between items-center">
                        <img src={`${userState.user?.image}`} className="rounded-full" alt="Timeline icon" width="40" height="40" />
                        <div className="flex-1 ml-1.2">
                            <input
                                type="text"
                                name="post"
                                value={description}
                                className="w-full bg-blue-100 sm:bg-gray-100 p-1.2 text-sm border border-gray-300 sm:border-gray-50 sm:border-2 rounded-md font-medium outline-none"
                                placeholder={`Voulez-vous dire quelque chose, ${userState.user?.firstName} ?`}
                                onChange={e => setDescription(e.target.value)}
                                ref={register && register({ required: true })}
                            />
                        </div>
                    </div>
                    <div className="flex justify-center mt-2">
                        <button type="submit" className="rounded-md bg-green-500 text-sm sm:text-base text-white-normal px-6 py-1.2">Publier</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default NoActualite