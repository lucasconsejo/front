import React, { useContext, useEffect, useState } from "react"
import { NavLink, useLocation } from "react-router-dom"
import TimelineIcon from "../../../assets/img/navbar/pc/ic_timeline.svg"
import KanbanIcon from "../../../assets/img/navbar/pc/ic_kanban.svg"
import TeamIcon from "../../../assets/img/navbar/pc/ic_team.svg"
import MessageIcon from "../../../assets/img/navbar/pc/ic_message.svg"
import NotificationIcon from "../../../assets/img/navbar/pc/ic_notification.svg"
import SettingsIcon from "../../../assets/img/navbar/pc/ic_settings.svg"

import TimelineActiveIcon from "../../../assets/img/navbar/pc/ic_timeline_active.svg"
import KanbanActiveIcon from "../../../assets/img/navbar/pc/ic_kanban_active.svg"
import TeamActiveIcon from "../../../assets/img/navbar/pc/ic_team_active.svg"
import MessageActiveIcon from "../../../assets/img/navbar/pc/ic_message_active.svg"

import { UserContext } from "../../../context/userContext"
import { userLogout } from "../../../api/auth"
import { useWindowResolution } from "../../../hooks/CustomHooks"

const Navbar: React.FC = () => {
    const [pathname, setPathname] = useState<string>("/")
    const { width } = useWindowResolution()
    const { userState } = useContext(UserContext)

    const location = useLocation()

    if (location.pathname.includes("settings")) {
        userLogout()
    }

    useEffect(() => {
        setPathname(location.pathname)
    }, [location])

    return (
        <nav className="w-full flex justify-between fixed items-center bg-white-normal px-4 shadow z-50">
            <NavLink className="flex-1" exact to="/timelines">
                <h1 className="text-2xl font-bold text-blue-600">Reactly</h1>
            </NavLink>

            <div className="flex flex-shrink-0 justify-between items-center">
                <NavLink activeClassName="hover:bg-blue-navbar-active-hover border-blue-600" className="px-12 py-2 border-b-2 border-transparent hover:bg-blue-navbar-hover" to="/timelines">
                    {
                        (pathname.includes("/timelines/post") || pathname === "/timelines") ? (
                            <img src={TimelineActiveIcon} alt="Timeline icon" width="30" height="30" />
                        ) : (
                            <img src={TimelineIcon} alt="Timeline icon" width="30" height="30" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="hover:bg-blue-navbar-active-hover border-blue-600" className="px-12 py-2 border-b-2 border-transparent hover:bg-blue-navbar-hover" to="/kanban">
                    {
                        (pathname.includes("/kanban")) ? (
                            <img src={KanbanActiveIcon} alt="Kanban icon" width="30" height="30" />
                        ) : (
                            <img src={KanbanIcon} alt="Kanban icon" width="30" height="30" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="hover:bg-blue-navbar-active-hover border-blue-600" className="px-12 py-2 border-b-2 border-transparent hover:bg-blue-navbar-hover" to="/team">
                    {
                        (pathname === "/team") ? (
                            <img src={TeamActiveIcon} alt="Team icon" width="30" height="30" />
                        ) : (
                            <img src={TeamIcon} alt="Team icon" width="30" height="30" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="hover:bg-blue-navbar-active-hover border-blue-600" className="px-12 py-2 border-b-2 border-transparent hover:bg-blue-navbar-hover" to="/message">
                    {
                        (pathname === "/message") ? (
                            <img src={MessageActiveIcon} alt="Message icon" width="30" height="30" />
                        ) : (
                            <img src={MessageIcon} alt="Message icon" width="30" height="30" />
                        )
                    }
                </NavLink>
            </div>

            <div className="flex flex-1 justify-end items-center">
                <NavLink to="/profil">
                    <div className="flex justify-between items-center">
                        <img src={`${userState.user?.image}`} className="rounded-full" alt="Profil icon" width="40" height="40" />
                        {(width > 1200) && <p className="text-xl mx-1.5">{userState.user?.firstName} {userState.user?.lastName}</p>}
                    </div>
                </NavLink>

                <NavLink className="mx-1" to="/notifications">
                    <img src={NotificationIcon} alt="Notification icon" width="40" height="40" />
                </NavLink>

                <NavLink to="/settings">
                    <img src={SettingsIcon} alt="Settings icon" width="40" height="40" />
                </NavLink>
            </div>
        </nav>
    )
}

export default Navbar