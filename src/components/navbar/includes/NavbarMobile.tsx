import React, { useContext, useEffect, useState } from "react"
import { NavLink, useLocation } from "react-router-dom"
import HideOnScroll from "../../../utils/HideOnScroll"
import TimelineIcon from "../../../assets/img/navbar/mobile/ic_timeline_mobile.svg"
import KanbanIcon from "../../../assets/img/navbar/mobile/ic_kanban_mobile.svg"
import TeamIcon from "../../../assets/img/navbar/mobile/ic_team_mobile.svg"
import MessageIcon from "../../../assets/img/navbar/mobile/ic_message_mobile.svg"
import MenuIcon from "../../../assets/img/navbar/mobile/ic_menu_mobile.svg"

import TimelineActiveIcon from "../../../assets/img/navbar/mobile/ic_timeline_mobile_active.svg"
import KanbanActiveIcon from "../../../assets/img/navbar/mobile/ic_kanban_mobile_active.svg"
import TeamActiveIcon from "../../../assets/img/navbar/mobile/ic_team_mobile_active.svg"
import MessageActiveIcon from "../../../assets/img/navbar/mobile/ic_message_mobile_active.svg"
import MenuActiveIcon from "../../../assets/img/navbar/mobile/ic_menu_mobile_active.svg"

import { UserContext } from "../../../context/userContext"
import { userLogout } from "../../../api/auth"

const NavbarMobile: React.FC = () => {
    const [pathname, setpathname] = useState<string>("/")
    const { userState } = useContext(UserContext)

    const location = useLocation()

    if (pathname.includes("profil")) {
        userLogout()
    }

    useEffect(() => {
        setpathname(location.pathname)
    }, [location])

    return (
        <HideOnScroll variant="down">
            <nav className="w-full z-10 flex justify-between fixed items-center bg-blue-600 shadow-lg z-50">
                <NavLink activeClassName="bg-blue-navbar-mobile-active" className="w-full py-1.5" to="/timelines">
                    {
                        (pathname.includes("/timelines/post") || pathname === "/timelines") ? (
                            <img src={TimelineActiveIcon} className="w-5 m-auto" alt="Timeline icon" />
                        ) : (
                            <img src={TimelineIcon} className="w-5 m-auto" alt="Timeline icon" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="bg-blue-navbar-mobile-active" className="w-full py-1.5" to="/kanban">
                    {
                        (pathname.includes("/kanban")) ? (
                            <img src={KanbanActiveIcon} className="w-5 m-auto" alt="Kanban icon" />
                        ) : (
                            <img src={KanbanIcon} className="w-5 m-auto" alt="Kanban icon" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="bg-blue-navbar-mobile-active" className="w-full py-1.5" to="/team">
                    {
                        (pathname === "/team") ? (
                            <img src={TeamActiveIcon} className="w-5 m-auto" alt="Team icon" />
                        ) : (
                            <img src={TeamIcon} className="w-5 m-auto" alt="Team icon" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="bg-blue-navbar-mobile-active" className="w-full py-1.5" to="/message">
                    {
                        (pathname === "/message") ? (
                            <img src={MessageActiveIcon} className="w-5 m-auto" alt="Message icon" />
                        ) : (
                            <img src={MessageIcon} className="w-5 m-auto" alt="Message icon" />
                        )
                    }
                </NavLink>
                <NavLink activeClassName="bg-blue-navbar-mobile-active" className="w-full py-1.5" to="/profil">
                    {
                        (pathname === "/profil") ? (
                            <img src={MenuActiveIcon} className="w-5 m-auto" alt="Message icon" />
                        ) : (
                            <img src={MenuIcon} className="w-5 m-auto" alt="Message icon" />
                        )
                    }
                </NavLink>
            </nav>
        </HideOnScroll>
    )
}

export default NavbarMobile