import React, { useState, useEffect } from "react"

export const useWindowResolution = () => {
    const [width, setWidth] = React.useState(window.innerWidth)
    const [height, setHeight] = React.useState(window.innerHeight)

    const updateWidthAndHeight = () => {
        setWidth(window.innerWidth)
        setHeight(window.innerHeight)
    }

    window.addEventListener("resize", updateWidthAndHeight)

    useEffect(() => {
        window.removeEventListener("resize", updateWidthAndHeight)
    }, [])

    return {
        width,
        height
    }
}