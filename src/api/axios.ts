import axios from "axios"
import { userAuth, userLogout, userRefreshToken, userToken } from "./auth"
import { DEBUG, API_BASE_URL } from "../utils/Utils"

const HEADER_AUTH_TOKEN = "X-REACTLY-AUTH-TOKEN"

axios.defaults.baseURL = API_BASE_URL
axios.defaults.headers.common["Content-Type"] = "application/json"
axios.defaults.headers.common.Accept = "application/json"

const updateToken = async (message?: string) => {
    const localRefreshToken = userRefreshToken()
    if (localRefreshToken === null || localRefreshToken === undefined || message?.length) {
        userLogout()
        window.location.replace("/login")
    }

    const response = await axios({
        method: "post",
        url: "/auth/token",
        data: {
            token: localRefreshToken
        }
    })

    const { accessToken } = response.data.data
    userAuth(accessToken, localRefreshToken!)
    return accessToken
}

// Request
axios.interceptors.request.use(
    config => {
        config.headers[HEADER_AUTH_TOKEN] = userToken()

        if (DEBUG) {
            console.info("➡️ Request ✅", config)
        }
        return config
    },
    error => {
        if (DEBUG) {
            console.error("➡️ Request ️❌", error)
        }
        throw new Error(error)
    }
)

// Response
axios.interceptors.response.use(
    response => {
        if (DEBUG) {
            console.info("⬅️️ Response ✅", response)
        }

        if (response.data.message === "RefreshToken not valid") {
            return updateToken(response.data.message).then(accessToken => {
                response.config.headers[HEADER_AUTH_TOKEN] = accessToken
                return axios.request(response.config)
            }).catch(err => {
                return axios.request(response.config)
            })
        }

        if (response.data.message === "Token expired" || response.data.message === "Token not valid") {
            return updateToken().then(accessToken => {
                response.config.headers[HEADER_AUTH_TOKEN] = accessToken
                return axios.request(response.config)
            }).catch(err => {
                return axios.request(response.config)
            })
        }

        if (response.data.status !== "success") throw response.data.message

        return response
    },
    error => {
        if (error.response !== undefined) {
            if (error.response.data.message === "RefreshToken not valid") {
                return updateToken(error.response.data.message).then(accessToken => {
                    error.config.headers[HEADER_AUTH_TOKEN] = accessToken
                    return axios.request(error.config)
                }).catch(err => {
                    return axios.request(error.config)
                })
            }

            if (error.response.data.message === "Token expired" || error.response.data.message === "Token not valid") {
                return updateToken().then(accessToken => {
                    error.config.headers[HEADER_AUTH_TOKEN] = accessToken
                    return axios.request(error.config)
                }).catch(err => {
                    return axios.request(error.config)
                })
            }

            if (DEBUG) {
                console.info("⬅️️ Response ️❌", error)
            }
        }
        throw error.response
    }
)

export default axios
