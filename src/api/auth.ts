import Cookies from "js-cookie"

export const checkUserAuth = () => Cookies.get("accessToken") !== undefined

export const userToken = () => Cookies.get("accessToken")

export const userRefreshToken = () => Cookies.get("refreshToken")

export const userLogout = () => {
    Cookies.remove("accessToken", { path: "/" })
    Cookies.remove("refreshToken", { path: "/" })
}

export const userAuth = (accessToken: string, refreshToken: string) => {
    Cookies.set("accessToken", accessToken, { path: "/" })
    Cookies.set("refreshToken", refreshToken, { path: "/" })
}
