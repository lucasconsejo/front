import { TaskType } from "../models/Types"
import axios from "./axios"

// Auth
export const loginApi = (email: string, password: string) => axios.post("/auth/login", { email, password })
export const registerApi = (email: string, password: string, firstName: string, lastName: string, job: string, team: string) => axios.post("/auth/register", { firstName, lastName, job, team, email, password })
export const allTeamApi = () => axios.get("/teams")

// User
export const getUser = () => axios.get("/users")

// Post
export const getAllPost = (page: number) => axios.get(`/posts?page=${page}`)
export const getPost = (id: string) => axios.get(`/posts/${id}`)
export const newPostApi = (formData: FormData) => axios.post("/posts", formData)
export const likePostApi = (id: string) => axios.patch(`/posts/${id}/like`)
export const deleteOnePost = (id: string) => axios.delete(`/posts/${id}`)
export const postComment = (comment: string, postId: string) => axios.post(`/posts/${postId}/comment`, { comment })