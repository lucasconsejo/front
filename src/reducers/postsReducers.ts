/* eslint-disable no-case-declarations */
export const initState = {
    posts: null,
    hasNextPage: false,
    loadMorePost: false
}

export const postsReducer = (state: any, action: any) => {
    switch (action.type) {
    case "INIT_POST":
        return {
            ...state,
            posts: action.payload
        }
    case "ADD_POST":
        const arrTempAddPost = state.posts
        arrTempAddPost.unshift(action.payload)
        return {
            ...state,
            posts: arrTempAddPost
        }
    case "UPDATE_POSTS":
        return {
            ...state,
            posts: [...state.posts, ...action.payload]
        }
    case "UPDATE_POST": {
        const arrTempLikedPost = state.posts
        const postLikedIndex = arrTempLikedPost.findIndex((post: any) => post._id === action.payload.postId)
        arrTempLikedPost[postLikedIndex] = action.payload.post
        return {
            ...state,
            posts: arrTempLikedPost
        }
    }
    case "DELETE_POST": {
        const arrTempDeletePost = state.posts
        const postDeletedIndex = arrTempDeletePost.findIndex((post: any) => post._id === action.payload)
        if (postDeletedIndex > -1) {
            arrTempDeletePost.splice(postDeletedIndex, 1)
        }
        return {
            ...state,
            posts: arrTempDeletePost
        }
    }
    case "HAS_NEXT_PAGE":
        return {
            ...state,
            hasNextPage: action.payload
        }
    case "LOAD_MORE_POST":
        return {
            ...state,
            loadMorePost: action.payload
        }
    default:
        return initState
    }
}
