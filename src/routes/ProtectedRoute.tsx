import React from "react"
import { Route, Redirect } from "react-router-dom"
import { checkUserAuth } from "../api/auth"

const ProtectedRoute = ({ component: Component, path, ...rest }: any) => {
    return (
        <Route path={path} {...rest}
            render={props => {
                return checkUserAuth() ? <Component {...props} /> : <Redirect to={{ pathname: "/login", state: { msg: "Vous devez être connecté pour accéder à cette page" } }} />
            }}
        />
    )
}

export default ProtectedRoute
