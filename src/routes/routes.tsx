import React from "react"
import loadable from "loadable-components"
import { Redirect, Route, Switch } from "react-router-dom"
import ProtectedRoute from "./ProtectedRoute"
import SharedPost from "../components/pages/timelines/SharedPost"

const Loading = () => <div />

export const Auth = loadable(() => import("../components/pages/auth"), {
    LoadingComponent: Loading
})
export const Timelines = loadable(() => import("../components/pages/timelines"), {
    LoadingComponent: Loading
})
export const Kanban = loadable(() => import("../components/pages/kanban"), {
    LoadingComponent: Loading
})

export const Team = loadable(() => import("../components/pages/team"), {
    LoadingComponent: Loading
})

export const Message = loadable(() => import("../components/pages/message"), {
    LoadingComponent: Loading
})

export const Profil = loadable(() => import("../components/pages/profil"), {
    LoadingComponent: Loading
})

export const Notifications = loadable(() => import("../components/pages/notifications"), {
    LoadingComponent: Loading
})

export const Settings = loadable(() => import("../components/pages/settings"), {
    LoadingComponent: Loading
})

Auth.load()
Timelines.load()
Kanban.load()
Team.load()
Message.load()
Profil.load()
Notifications.load()
Settings.load()

const Routes: React.FC = () => (
    <Switch>
        <ProtectedRoute exact path="/timelines/post/:postId" component={SharedPost} />
        <ProtectedRoute exact path="/timelines" component={Timelines} />
        <ProtectedRoute exact path="/kanban/:id" component={Kanban} />
        <ProtectedRoute exact path="/kanban" component={Kanban} />
        <ProtectedRoute exact path="/team" component={Team} />
        <ProtectedRoute exact path="/message" component={Message} />
        <ProtectedRoute exact path="/profil" component={Profil} />
        <ProtectedRoute exact path="/notifications" component={Notifications} />
        <ProtectedRoute exact path="/settings" component={Settings} />
        <Route exact path="/login" component={Auth} />
        <Route exact path="/register" component={Auth} />
        <Redirect to="/timelines" />
    </Switch>
)

export default Routes