import React, { useReducer } from "react"
import { initState, tablesReducer } from "../reducers/tablesReducers"
import { TableType } from "../models/Types"

const defaultValueType: TablesContextType = {
    tablesState: initState,
    tablesDispatch: () => null
}

type PropsType = {
    children: any
}

export interface TablesContextType {
    tablesState: { tables: Array<TableType> },
    tablesDispatch: any
}

export const TablesContext = React.createContext<TablesContextType>(defaultValueType)

const TablesProvider: React.FC<PropsType> = ({ children }) => {
    const [tablesState, tablesDispatch] = useReducer(tablesReducer, initState)

    return (
        <TablesContext.Provider value={{ tablesState, tablesDispatch }}>
            { children }
        </TablesContext.Provider>
    )
}

export default TablesProvider
