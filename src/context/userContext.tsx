import React, { useReducer, useState } from "react"
import { initState, userReducer } from "../reducers/userReducers"
import { UserType } from "../models/Types"
import { checkUserAuth } from "../api/auth"
import { getUser } from "../api/ApiRequest"
import { useComponentWillMount } from "../utils/Utils"

const defaultValueType: UserContextType = {
    userState: initState,
    userDispatch: () => null
}

type PropsType = {
    children: any
}

export interface UserContextType {
    userState: { user: UserType | null },
    userDispatch: any
}

export const UserContext = React.createContext<UserContextType>(defaultValueType)

const UserProvider: React.FC<PropsType> = ({ children }) => {
    const [userState, userDispatch] = useReducer(userReducer, initState)
    const [isLoading, setIsLoading] = useState<boolean>(true)

    useComponentWillMount(async () => {
        if (isLoading) {
            if (checkUserAuth()) {
                if (userState.user === null) {
                    await getUser().then(res => {
                        userDispatch({
                            type: "ADD_USER",
                            payload: res.data.data
                        })
                    })
                }
            }
            setIsLoading(false)
        }
    })

    const isFinishLoadind = () => {
        if (!isLoading) {
            return children
        }
        return null
    }

    return (
        <UserContext.Provider value={{ userState, userDispatch }}>
            { isFinishLoadind() }
        </UserContext.Provider>
    )
}

export default UserProvider
