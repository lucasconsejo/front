export type PostType = {
    _id: string,
    user: UserForPostType,
    nbLike: number,
    description: string,
    images?: string[],
    comments: Array<CommentType>
    publicationDate: Date,
    currentUserLiked: boolean
}

export type CommentType = {
    id: string,
    userId: string,
    comment: string,
    user: UserForPostType,
    publicationDate: Date
}

export type UserForPostType = {
    id: string,
    name: string,
    job : string,
    image: string,
}

export type UserType = {
    _id: string,
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    team: string | null,
    teamId: string | null,
    job: string,
    image: string | null,
}

export type TableType = {
    _id: string,
    name: string,
    subTable: Array<SubTableType>
    tasks: Array<TaskType>,
    teamId: string
}

export type SubTableType = {
    _id: string,
    name: string,
    tableId: string
}

export type UserForTaskType = {
    id: string,
    name: string,
    job : string,
    image: string
}

export type TaskType = {
    _id: string,
    title: string,
    description: string,
    subTableId: string,
    user: UserForTaskType,
    label?: string,
    labelColor?: string,
    priority?: string,
    estimate?: string
}
