import firebase from "firebase"

const config = {
    apiKey: "AIzaSyCzojqbcs7Z2FbiIeEsGiuaJCC9cMnQpIo",
    authDomain: "reactly-ced4c.firebaseapp.com",
    projectId: "reactly-ced4c",
    storageBucket: "reactly-ced4c.appspot.com",
    messagingSenderId: "855496530617",
    appId: "1:855496530617:web:9a369873507ed2515bd968"
}

firebase.initializeApp(config)

export default firebase