import React, { useEffect, useState } from "react"

type PropsType = {
    children: any,
    variant: "up" | "down"
}

const HideOnScroll: React.FC<PropsType> = ({ variant, children }) => {
    const [show, setShow] = useState<boolean>(false)
    const [initShow, setInitShow] = useState<boolean>(true)
    const [prevYOffset, setPrevYOffset] = useState<number>(window.pageYOffset)

    const listenToScroll = () => {
        const currentYOffset = window.pageYOffset

        if (currentYOffset <= 50) {
            setInitShow(true)
        }
        else if (prevYOffset > currentYOffset) {
            setShow(variant === "down")
            setPrevYOffset(currentYOffset)
            setInitShow(false)
        } else {
            setShow(variant !== "down")
            setPrevYOffset(currentYOffset)
            setInitShow(false)
        }
    }

    useEffect(() => {
        window.addEventListener("scroll", listenToScroll)

        return () => {
            window.removeEventListener("scroll", listenToScroll)
        }
    }, [listenToScroll])

    return (initShow) ? children : (
        <div className={`show-${show ? "up" : "down"}`}>
            { children }
        </div>
    )
}

export default HideOnScroll
