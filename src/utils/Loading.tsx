import React from "react"
import ReactLoading from "react-loading"

const Loading: React.FC = () => {
    return (
        <div className="justify-center items-start pt-40 bg-gray-500 bg-opacity-40 flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="flex items-center justify-center bg-white-normal rounded-md p-2">
                <ReactLoading type="spin" color="#4848ff" height="50px" width="50px" />
            </div>
        </div>
    )
}

export default Loading
