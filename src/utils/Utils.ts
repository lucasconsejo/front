/* eslint-disable no-plusplus */
import { useMemo } from "react"
import moment from "moment"
import "moment/locale/fr"

export const useComponentWillMount = (func: () => unknown) => {
    useMemo(func, [func])
}

export const DEBUG = (process.env.REACT_APP_ENV !== undefined && process.env.REACT_APP_ENV === "dev")

export const API_BASE_URL = process.env.REACT_APP_API_URL ? `https://${process.env.REACT_APP_API_URL}` : "http://localhost:8080"
export const APP_BASE_URL = !DEBUG ? "https://reactly.fr" : "http://localhost:3000"

export const capitalize = (value: string | null | undefined) => {
    if (value === null || value === "") {
        return ""
    }
    return value![0].toUpperCase() + value!.slice(1)
}

export const dateFormat = (date: Date) => {
    const currentDate = moment(new Date())
    const newDate = moment(date)

    if (currentDate.diff(newDate, "minutes") < 1 && newDate.format("DD") !== currentDate.format("DD")) return `Hier à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "minutes") < 60 && newDate.format("DD") !== currentDate.format("DD")) return `Hier à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "days") < 1 && newDate.format("DD") !== currentDate.format("DD")) return `Hier à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "minutes") < 1) return "A l'instant"
    if (currentDate.diff(newDate, "minutes") < 60) return `${currentDate.diff(newDate, "minutes")}m`
    if (currentDate.diff(newDate, "days") < 1) return `${currentDate.diff(newDate, "hours")}h`
    if (currentDate.diff(newDate, "days") < 2) return `${newDate.format("DD")} ${capitalize(newDate.format("MMMM"))} à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "months") <= 1 && currentDate.diff(newDate, "months") < 12 && newDate.format("YYYY") !== currentDate.format("YYYY")) return `${newDate.format("DD")} ${capitalize(newDate.format("MMMM"))} ${newDate.format("YYYY")} à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "months") <= 1 && currentDate.diff(newDate, "months") < 12) return `${newDate.format("DD")} ${capitalize(newDate.format("MMMM"))} à ${newDate.format("HH")}h${newDate.format("MM")}`
    if (currentDate.diff(newDate, "years") >= 1) return `${newDate.format("DD")} ${capitalize(newDate.format("MMMM"))} ${newDate.format("YYYY")}`
    return null
}

export const getRandomColor = () => {
    const letters = "0123456789ABCDEF"
    let color = "#"
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)]
    }
    return color
}

export const priorityStatus = [
    "",
    "blocker",
    "critical",
    "high",
    "highest",
    "low",
    "lowest",
    "major",
    "medium",
    "minor",
    "trivial"
]