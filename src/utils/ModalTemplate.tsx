import React from "react"

type Props = {
    title: string,
    showModal: any
}

const ModalTemplate: React.FC<Props> = ({ title, children, showModal }) => (
    <div>
        <div className="justify-center items-center bg-gray-300 bg-opacity-75 flex fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="flex-auto w-xl max-w-lg shadow-lg">
                <div className="px-1.5 border-0 rounded-lg shadow-lg flex flex-col w-full bg-white-normal outline-none focus:outline-none">
                    {/* header */}
                    <div className="p-1 py-1.2 flex justify-center items-center border-b border-solid border-gray-300 rounded-t">
                        <h3 className="w-full text-xl font-semibold text-center">{title}</h3>
                        <button
                            className="ml-auto bg-transparent border-0 text-black float-right text-2xl leading-none font-semibold outline-none focus:outline-none"
                            onClick={() => showModal(false)}
                        >
                            <span className="bg-transparent text-black text-3xl outline-none focus:outline-none">×</span>
                        </button>
                    </div>
                    {/* content */}
                    {children}
                </div>
            </div>
        </div>
        <div className="opacity-25 fixed inset-0 z-40 bg-black" />
    </div>
)

export default ModalTemplate