import React from "react"
import { shallow } from "enzyme"
import { createMemoryHistory } from "history"
import Auth from "../../../components/pages/auth"
import Login from "../../../components/pages/auth/includes/Login"
import Register from "../../../components/pages/auth/includes/Register"

jest.mock("axios")

describe("Auth", () => {
    const props = {
        location: createMemoryHistory().location
    }

    describe("Check routes", () => {
        it("Should render Login", () => {
            props.location.pathname = "/login"
            const wrapper = shallow(<Auth {...props} />)

            expect(wrapper.find("h1").text()).toBe("Reactly")
            expect(wrapper.find("div").at(0).contains(<Login />)).toEqual(true)
            expect(wrapper.find("div").at(0).contains(<Register />)).toEqual(false)
        })

        it("Should render Register", () => {
            props.location.pathname = "/register"
            const wrapper = shallow(<Auth {...props} />)

            expect(wrapper.find("h1").text()).toBe("Reactly")
            expect(wrapper.find("div").at(0).contains(<Login />)).toEqual(false)
            expect(wrapper.find("div").at(0).contains(<Register />)).toEqual(true)
        })
    })
})
