import React from "react"
import { BrowserRouter } from "react-router-dom"
import { cleanup, render, fireEvent, act } from "@testing-library/react"
import Login from "../../../components/pages/auth/includes/Login"
import { loginRequest } from "../../../components/pages/auth/includes/Requests"

jest.mock("../../../components/pages/auth/includes/Requests")
jest.mock("../../../api/ApiRequest")

describe("Login", () => {
    const emailMockValue = "Test@123"
    const passwordMockValue = "test"
    const mockPostData = {
        email: emailMockValue,
        password: passwordMockValue
    }

    afterEach(cleanup)

    test("should watch input correctly", () => {
        const { container } = render(
            <BrowserRouter>
                <Login />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")

        fireEvent.input(email!, {
            target: {
                value: emailMockValue
            }
        })
        fireEvent.input(password!, {
            target: {
                value: passwordMockValue
            }
        })

        expect(email.value).toEqual(emailMockValue)
        expect(password.value).toEqual(passwordMockValue)
    })

    test("should display correct error message for password miss match", async () => {
        const { container } = render(
            <BrowserRouter>
                <Login />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(password!, {
            target: {
                value: ""
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(container.textContent).toMatch(/Veuillez saisir un email/)
        expect(container.textContent).toMatch(/Veuillez saisir un mot de passe/)
    })

    it("Should submit form successfully", async () => {
        loginRequest.mockImplementationOnce(() => Promise.resolve(true))

        const { container } = render(
            <BrowserRouter>
                <Login />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: emailMockValue
            }
        })
        fireEvent.input(password!, {
            target: {
                value: passwordMockValue
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(loginRequest).toHaveBeenCalledWith(emailMockValue, passwordMockValue)
    })

    test("Should submit form error", async () => {
        loginRequest.mockImplementationOnce(() => Promise.reject(new Error("Une erreur est survenue")))
        const { container } = render(
            <BrowserRouter>
                <Login />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: emailMockValue
            }
        })
        fireEvent.input(password!, {
            target: {
                value: passwordMockValue
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(loginRequest).toHaveBeenCalledWith(emailMockValue, passwordMockValue)
        expect(container.textContent).toMatch("Une erreur est survenue")
    })
})