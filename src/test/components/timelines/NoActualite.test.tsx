import React from "react"
import axios from "axios"
import { shallow } from "enzyme"
import { newPostApi } from "../../../api/ApiRequest"
import { checkUserAuth } from "../../../api/auth"
import NoActualite from "../../../components/pages/timelines/includes/NoActualite"

jest.mock("axios")
jest.mock("../../../api/ApiRequest", () => ({
    newPostApi: jest.fn()
}))
jest.mock("../../../api/auth", () => ({
    checkUserAuth: jest.fn()
}))

describe("NoActualite", () => {
    it("Should render NoActualite", () => {
        checkUserAuth.mockReturnValueOnce(false)
        const wrapper = shallow(<NoActualite />)
    })
})
